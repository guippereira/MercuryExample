#ifndef CutsMercuryExample_H
#define CutsMercuryExample_H

#include "EventBase.h"

class CutsMercuryExample {

public:
    CutsMercuryExample(EventBase* eventBase);
    ~CutsMercuryExample();
    bool MercuryExampleCutsOK();

private:
    EventBase* m_event;
};

#endif
