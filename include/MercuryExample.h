#ifndef MercuryExample_H
#define MercuryExample_H

#include "Analysis.h"

#include "CutsMercuryExample.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class MercuryExample : public Analysis {

public:
    MercuryExample();
    ~MercuryExample();

    void Initialize();
    void Execute();
    void Finalize();

protected:
    CutsMercuryExample* m_cutsMercuryExample;
    ConfigSvc* m_conf;
};

#endif
