#include "CutsMercuryExample.h"
#include "ConfigSvc.h"

CutsMercuryExample::CutsMercuryExample(EventBase* eventBase)
{
    m_event = eventBase;
}

CutsMercuryExample::~CutsMercuryExample()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsMercuryExample::MercuryExampleCutsOK()
{
    // List of common cuts for this analysis into one cut
    return true;
}
