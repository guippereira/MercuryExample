#include "MercuryExample.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsMercuryExample.h"
#include "EventBase.h"
#include "Mercury.h" // need to add this line
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

// Constructor
MercuryExample::MercuryExample()
    : Analysis()
{
    // Set up the expected event structure, include branches required for analysis.
    // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
    // Load in the Single Scatters Branch
    m_event->IncludeBranch("ss");
    m_event->Initialize();
    ////////
    m_mercury->setTopLRFPath("/cvmfs/lz.opensciencegrid.org/Physics/Components/LRF-10Oct22/Top_10Oct22_Kr83m_preSR2-II.json");
    m_mercury->loadLRFs();

    //Disable PMTs with specific indexs 
    //m_mercury->setTopDisabledList([0,1,2,3,5,20]);
    //m_mercury->setBottomDisabledList([0,1,2,3,5,20]);

    // Setup logging
    logging::set_program_name("MercuryExample Analysis");

    // Setup the analysis specific cuts.
    m_cutsMercuryExample = new CutsMercuryExample(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();
}

// Destructor
MercuryExample::~MercuryExample()
{
    delete m_cutsMercuryExample;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void MercuryExample::Initialize()
{
    INFO("Initializing MercuryExample Analysis");
}

// Execute() - Called once per event.
void MercuryExample::Execute()
{

    int numSS = (*m_event->m_singleScatter)->nSingleScatters;
    if (numSS > 0) {
        //get pulse id
        int s2_id = (*m_event->m_singleScatter)->s2PulseID;

        //reconstruct position of specific pulse in event
        m_mercury->processPulseId(s2_id);
        
        //Alternativly you can provide you custom pulse HitPattern. 
        //In this case you need to input the PMT pulse area and a boolean flag to indicate the PMT saturation and exlude it from reconstruction. 
        //Two vectors (float and boolenas) with the same size that of number of PMTs in the top array need to be provided.
        //m_mercury-processHitPattern([pmt0, pmt1...pmt252], [satflag0, satflag1...satflag252 ]);
        
        //Extract reconstruction results. 
        std::cout << "S2 id: " << s2_id << " X: " << m_mercury->getRecX() << " Y: " << m_mercury->getRecY()  << std::endl; 


        //Full list of available RQs:
        /*
            float getRecX() // event x position in mm
            float getRecY() // event y position in mm
            int getRecStatus() // get reconstruction status (0 is good)
            float getDof() // number of PMT used in reconstruction
            float getChi2Min() // chi2 of minimization
            float getRedChi2() // reduced chi2 of minimization
            float getCovXX() //covaraince matrix results
            float getCovYY() //covaraince matrix results
            float getCovXY() //covaraince matrix results
            float getCorrectedTopArea() //Light collection correction for top array
            float getCorrectedBottomArea() //Light collection correction for bottom array
        */

    }
}

// Finalize() - Called once after event loop.
void MercuryExample::Finalize()
{
    INFO("Finalizing MercuryExample Analysis");
}
