# MercuryExample

This module serves as a starting point to run Alpaca with Mercury.
You should create your own module via the `makeAnalysisModule` command.

Before starting, you should confirm that the version of your AlpacaCore is prepared to run Mercury. Some simple check are:
 1. Confirm that the files `modules/AlpacaCore/src/Mercury.cxx` and `modules/AlpacaCore/include/Mercury.h` exists.
 2. Confirm that the [mercury library](https://gitlab.com/vovalz/mercury) is installed in `modules/AlpacaCore/mercury`.
 3. The file `modules/AlpacaCore/CMakeLists.txt` should contain something similar to this:
	```
	$ENV{EIGEN3_INCLUDE_DIR}
	$ENV{MERCURYLIB}
	$ENV{MERCURYLIB}/LRModel
	$ENV{MERCURYLIB}/spline123
	```
If these components exists and were well setup you should be good to go.
## Getting started


1. Update the source file of your analysis under `modules/<module_name>/src/`.

	```cpp
	#include "EventBase.h"
	#include "Mercury.h" // <---- need to add this line
	#include "HistSvc.h"
	```

2. Update the config file of your module under `modules/<module_name>/config>`:

	```
	outName			MercuryExampleAnalysis.root
	level 			3 # Logging level: error = 1, warning = 2, info = 3, debug = 4, verbose = 5
	fileList        	MercuryExampleInputFiles.list
	whichAna		0 #This is the principle analysis 
	MercuryState         1 # <----- Add this to activate activate mercury 
	``` 

3. As per the code in the source file of this example, first you need to load the LRFs with `m_mercury->loadLRFs();`. 

Afterwards Mercury is ready to reconstruct the position of a given pulse with `m_mercury->processPulseId(s2_id);` or with `m_mercury-processHitPattern([pmt0, pmt1...pmt252], [satflag0, satflag1...satflag252 ]);`.

Feel free to contact Guilherme Pereira if you have any problems, questions or suggestions.